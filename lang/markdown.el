;;; lang/markdown.el -*- lexical-binding: t; -*-

(eval-after-load 'markdown-mode
  ;; Automatically use electric-pair-mode
  (electric-pair-mode 1))

(auto-fill-mode 1)


;; Autocompletion of stars used by italic
(defvar markdown-electric-pairs '((?* . ?*)) "Electric pairs for markdown-mode.")
(defun markdown-add-electric-pairs ()
  (setq-local electric-pair-pairs (append electric-pair-pairs markdown-electric-pairs))
  (setq-local electric-pair-text-pairs electric-pair-pairs))

(add-hook 'markdown-mode-hook 'markdown-add-electric-pairs)

(defun markdown-insert-comment ()
  "Insert a portable markdown comment. See https://stackoverflow.com/a/20885980/954777."
  (interactive)
  (insert "[//]: # ()"))

(defun markdown-insert-comment-around-region (start end)
  "Insert a comment around a region."
  (interactive "r")
  (save-excursion
    (goto-char end) (insert ")")
    (goto-char start) (insert "[//]: # (")))

(global-set-key (kbd "C-c m") 'markdown-insert-comment-around-region)
