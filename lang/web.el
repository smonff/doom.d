;;; lang/web.el -*- lexical-binding: t; -*-

(add-to-list 'auto-mode-alist '("\\.tt\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ep\\'" . web-mode))

(setq web-mode-markup-indent-offset 2)
(setq web-mode-css-indent-offset 2)
(setq web-mode-code-indent-offset 2)

(setq web-mode-enable-auto-pairing t)

(setq web-mode-enable-css-colorization t)
(setq web-mode-enable-auto-indentation t)
