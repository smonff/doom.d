;;; lang/perl.el -*- lexical-binding: t; -*-

;;; cperl-mode is preferred to perl-mode
;;; "Brevity is the soul of wit" <foo at acm.org>
(defalias 'perl-mode 'cperl-mode)

(add-hook! 'cperl-mode-hook 'hasklig-mode)
(add-hook! 'cperl-mode-hook 'auto-complete-mode)

(custom-set-variables
 '(cperl-indent-level 2)
 '(cperl-indent-parens-as-block t)
 '(cperl-continued-statement-offset 2))

;
; mmm-mode config for Mojolicious::Lite perl file.
; Forked from https://gist.github.com/am0c/3803249
; mmm-mode lib is from here: https://github.com/purcell/mmm-mode
; font locks of [c]perl-mode and mmm-mode conflict each other.
;
(require 'mmm-auto)
(require 'mmm-compat)
(require 'mmm-vars)

(setq mmm-global-mode 'maybe)

(global-set-key "\M-p" 'mmm-parse-buffer)

;(mmm-set-major-mode-preference 'cperl-mode 'perl-mojo)
(defun mmm-perl-mojo-get-mode (delimiter)
  (let ((filename (substring delimiter 3 nil)))
    (or (assoc-default filename auto-mode-alist #'string-match)
        'fundamental-mode
        (signal 'mmm-no-matching-submode nil))))

(mmm-add-group 'perl-mojo '((perl-mojo-blocks
                             :match-submode mmm-perl-mojo-get-mode
                             :front "^@@ \\(.+\\)$"
                             :front-offset (end-of-line 1)
                             :back "\\(^@@\\|\\'\\)"
                             :back-offset (beginning-of-line -1)
                             :save-matches t
                             :delimiter-mode nil
                             :end-not-begin nil
                             :face mmm-code-submode-face
                             )))

(mmm-add-mode-ext-class 'cperl-mode "\\.pl$" 'perl-mojo)
(setq mmm-submode-mode-line-format "~M[~m]")
(setq mmm-submode-decoration-level 1)

(add-to-list 'auto-mode-alist '("\\.pl\\'" . cperl-mode))
(add-to-list 'auto-mode-alist '("\\.pm\\'" . cperl-mode))
(add-to-list 'auto-mode-alist '("\\cpanfile\\'" . cperl-mode))
(add-to-list 'auto-mode-alist '("\\.t\\'" . cperl-mode))
(add-to-list 'auto-mode-alist '("\\.zydeco\\'" . cperl-mode))


;; Makes possible for Flycheck to find the local libs of a project. Without
;; this, the modules in lib/ are not found and it is asked they are installed.
;; Require to install Project::Libs globally
;;
;; See https://m0t0k1ch1st0ry.com/blog/2014/07/07/flycheck/
;;
;; https://randstructure.wordpress.com/2015/06/21/using-emacs-flycheck-with-perl-and-perlbrew/
;; can also be an alternative.
(flycheck-define-checker perl-project-libs
  "A perl syntax checker."
  :command ("perl"
            "-MProject::Libs lib_dirs => [qw(local/lib/perl5)]"
            "-wc"
            source-inplace)
  :error-patterns ((error line-start
                          (minimal-match (message))
                          " at " (file-name) " line " line
                          (or "." (and ", " (zero-or-more not-newline)))
                          line-end))
  :modes (cperl-mode))


(add-hook 'cperl-mode-hook
          (lambda ()
            (flycheck-mode t)
            (setq flycheck-checker 'perl-project-libs)))
