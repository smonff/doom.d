;;; navigation.el -*- lexical-binding: t; -*-

;; Some navigation keybindings that needed to be redefined

;; Simplier than using ^
(map! :map dired-mode-map
  "<prior>" 'dired-up-directory
  "\\" 'dired-up-directory
  "<next>" 'dired-find-file )

;; Similar to (global-set (kbd "C-x ,") 'previous-buffer)
(map! :g "C-x ." 'next-buffer)
(map! :g "C-x <right>" 'ignore)
(map! :g "C-x ," 'previous-buffer)
(map! :g "C-x <left>" 'ignore)

;; Scroll the page with in-place point
;; Those are functions, not a module
(defun gcm-scroll-down ()
  (interactive)
  (scroll-up 1))

(defun gcm-scroll-up ()
  (interactive)
  (scroll-down 1))

(map! :g "M-\\" 'gcm-scroll-up)
(map! :g "C-\\" 'gcm-scroll-down)

;; Handy shortcut for changing buffer without C-x o
(map! "<f9>" 'other-window)
(map! "<M-tab>" 'other-window)

;; narrow dired to match filter
(map! :map dired-mode-map
  "/" 'dired-narrow)

;; Navigation between windows that just works
(map! :g "C-c <right>" 'windmove-right)
(map! :g "C-c <left>" 'windmove-left)
(map! :g "C-c <up>" 'windmove-up)
(map! :g "C-c <down>" 'windmove-down)

;; More standard way to navigate through auto-complete menus
;; Remember that the right candidate can be selected using M-nth (e.g. M-1, M-2, etc.)
;; https://github.com/auto-complete/auto-complete/blob/master/doc/manual.md#select-candidates-with-c-nc-p-only-when-completion-menu-is-displayed
(setq ac-use-menu-map t)
;; Default settings
(define-key ac-menu-map "\C-n" 'ac-next)
(define-key ac-menu-map "\C-p" 'ac-previous)
