;;; ui.el -*- lexical-binding: t; -*-

(add-to-list 'initial-frame-alist '(fullscreen . fullboth))

;; The fringe, with a large one on the left and a small one on the right
(after! git-gutter-fringe
  (fringe-mode '20))

;;(after! git-gutter-fringe
  ;;(fringe-mode '(30 . 5))
