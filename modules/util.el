;;; util.el -*- lexical-binding: t; -*-

;; Turn a two steps command in a one step command
(defun ispell-turn-to-french ()
  "Two commands in only one for loading the French Ispell dictionary."
  (interactive)
  (ispell-change-dictionary "francais"))

(require 'wakatime-mode)
(setq wakatime-api-key "6ec2875e-c45d-4098-883c-bfd23afbabc2")
(setq wakatime-cli-path "/usr/local/bin/wakatime")
(global-wakatime-mode)
