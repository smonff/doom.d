;;; typo.el -*- lexical-binding: t; -*-

;; Typographic stuff

(defun insert-middle-dot ()
  "Insert a middle dot"
  (interactive)
  (insert "··"))


(defun insert-non-breakable-space ()
  "Insert a non-breakable space"
  (interactive)
  ;; C-q 2 4 0 RET would be better
  (insert " "))

(global-set-key (kbd "C-c SPC") 'insert-non-breakable-space)

(defun insert-non-breakable-spaces-around-region (start end)
  "Insert a typographic symbols around a region."
  (interactive "r")
  (save-excursion
    (goto-char end) (insert " »*")
    (goto-char start) (insert "*« ")))

(global-set-key (kbd "C-c /") 'insert-non-breakable-spaces-around-region)


(defun insert-ae ()
  "Insert the e in a symbol for inclusive writing."
  (interactive)
  (insert "æ"))
